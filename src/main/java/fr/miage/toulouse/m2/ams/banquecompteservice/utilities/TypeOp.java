package fr.miage.toulouse.m2.ams.banquecompteservice.utilities;

public enum TypeOp {
    CREDIT, DEBIT
}
