package fr.miage.toulouse.m2.ams.banquecompteservice.metier;

import fr.miage.toulouse.m2.ams.banquecompteservice.dao.Compte;
import fr.miage.toulouse.m2.ams.banquecompteservice.repo.CompteRepository;
import fr.miage.toulouse.m2.ams.banquecompteservice.utilities.CompteDupliqueException;
import fr.miage.toulouse.m2.ams.banquecompteservice.utilities.CompteInconnuException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class Metier {

    private final CompteRepository compteRepository;

    public Metier(CompteRepository compteRepository) {
        this.compteRepository = compteRepository;
    }

    public Compte crediter(Long idcompte, double montant) throws CompteInconnuException {
        Compte c = compteRepository.findById(idcompte).orElseThrow(CompteInconnuException::new);
        c.setSolde(c.getSolde() + montant);
        compteRepository.save(c);
        return c;

    }

    public Compte debiter(Long idcompte, double montant) throws CompteInconnuException {
        Compte c = compteRepository.findById(idcompte).orElseThrow(CompteInconnuException::new);
        c.setSolde(c.getSolde() - montant);
        compteRepository.save(c);
        return c;

    }

    public Compte consulterCompte(Long idcompte) throws CompteInconnuException {
        return compteRepository.findById(idcompte).orElseThrow(CompteInconnuException::new);
    }

    public void transferer(Long idcompteSrc, Long idcompteDest, double montant) throws CompteInconnuException {
        debiter(idcompteSrc, montant);
        crediter(idcompteDest, montant);
    }

    public Compte ajouterCompte(Compte compte) throws CompteDupliqueException {
        if (compteRepository.existsById(compte.getId())) throw new CompteDupliqueException();
        return compteRepository.save(compte);
    }

    public void supprimerCompte(Long idcompte) {
        compteRepository.deleteById(idcompte);
    }

    public Compte modifierCompte(Long idcompte, Compte compte) throws CompteInconnuException {
        if (!compteRepository.existsById(idcompte)) throw new CompteInconnuException();
        compte.setId(idcompte);
        compteRepository.save(compte);
        return compte;
    }

    public List<Compte> listerComptes() {
        return compteRepository.findAll();
    }

    public List<Compte> listerComptesClient(Long idclient) {
        return compteRepository.findAllByIdclient(idclient);
    }
}
