package fr.miage.toulouse.m2.ams.banquecompteservice.utilities;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class CompteInconnuException extends Exception {
}
