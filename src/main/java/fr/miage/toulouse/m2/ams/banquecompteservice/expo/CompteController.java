package fr.miage.toulouse.m2.ams.banquecompteservice.expo;

import fr.miage.toulouse.m2.ams.banquecompteservice.dao.Compte;
import fr.miage.toulouse.m2.ams.banquecompteservice.metier.Metier;
import fr.miage.toulouse.m2.ams.banquecompteservice.utilities.BadParamException;
import fr.miage.toulouse.m2.ams.banquecompteservice.utilities.CompteDupliqueException;
import fr.miage.toulouse.m2.ams.banquecompteservice.utilities.CompteInconnuException;
import fr.miage.toulouse.m2.ams.banquecompteservice.utilities.TypeOp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * Service d'exposition REST des comptes.
 * URL / exposée.
 */
@RestController
/*
    AVANT : @RequestMapping("/api/comptes")
    Modif : plus besoin de /api/comptes via
    l'usage de l'API Gateway qui va masquer le chemin de l'URL
 */
@RequestMapping("/")
public class CompteController {
    Logger logger = LoggerFactory.getLogger(CompteController.class);

    // Injection DAO compte
    private final Metier metier;

    public CompteController(Metier metier) {
        this.metier = metier;
    }

    /**
     * GET 1 compte
     * @param compte id du compte
     * @return Compte converti en JSON
     */
    @GetMapping("{id}")
    public Compte getCompte(@PathVariable("id") Long id) throws CompteInconnuException {
        logger.info("Compte : demande d'un compte avec id:{}", id);
        return metier.consulterCompte(id);
    }

    /**
     * GET liste des comptes d'un client ou de tous les clients par défaut
     * @return liste des comptes en JSON. [] si aucun compte.
     */
    @GetMapping("")
    public List<Compte> getComptes(@RequestParam(value = "client") Optional<Long> id) {
        if (id.isEmpty()) {
            // on veut la liste totale
            logger.info("Compte : demande de la liste de tous les comptes");
            return metier.listerComptes();
        }
        else {
            // on veut la liste pour 1 seul client
            logger.info("Compte : demande des comptes d'un client avec id:{}", id.get());
            return metier.listerComptesClient(id.get());
        }
    }


    /**
     * POST un compte
     * @param cpt compte à ajouter (import JSON)
     * @return compte ajouté
     */
    @PostMapping("")
    public Compte postCompte(@RequestBody Compte cpt) throws CompteDupliqueException {
        logger.info("Compte : demande CREATION d'un compte avec id:{}", cpt.getId());
        return metier.ajouterCompte(cpt);
    }

    /**
     * PUT un compte
     * @param idCompte id du compte à modifier
     * @param typeoperation type de l'opration DEBIT ou CREDIT
     * @param montant montant de l'opération
     * @return compte modifié
     */
    @PutMapping("{id}")
    public Compte putCompte(@PathVariable("id") Long idCompte, @RequestParam(value = "op") TypeOp typeoperation, @RequestParam(value = "montant") Double montant) throws CompteInconnuException, BadParamException {
        logger.info("Compte : demande OPERATION sur un compte avec id:{}", idCompte);
        if (montant == null) {
            throw new BadParamException();
        }
        if (montant <= 0) {
            throw new BadParamException();
        }
        Compte c;
        switch (typeoperation) {
            case CREDIT: {
                c = metier.crediter(idCompte, montant);
                break;
            }
            case DEBIT: {
                c = metier.debiter(idCompte, montant);
                break;
            }
            default:
                throw new BadParamException();
        }
        return c;
    }

}
