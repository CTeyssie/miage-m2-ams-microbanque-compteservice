package fr.miage.toulouse.m2.ams.banquecompteservice.utilities;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class CompteDupliqueException extends Exception {
}
